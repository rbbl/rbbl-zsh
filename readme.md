# rbbl-zsh

## about
minimal custom zsh config with a couple of optional integrations along with a custom starship config.

optional integrations:
 - kubernetes completions
 - helm completions
 - minikube completions

screen shot:\
![screen shot](./screenshot.png)

## requirements

- zsh
- [starship prompt](https://starship.rs/guide/#%F0%9F%9A%80-installation)

## installation
make sure git and zsh are installed.

1. clone the git project into any folder
```shell
git clone https://gitlab.com/rbbl/rbbl-zsh.git
```

2. execute the installation script
```shell
./rbbl-zsh/install.zsh
```

3. (optional) activate integrations - edit the `.zshrc` in your home directory and add the integrations you want to be active to the plugins list like shown in the comment. 
