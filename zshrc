#History
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -e
#enables basic compeltion
autoload -Uz compinit && compinit

#Case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'

eval "$(starship init zsh)"

bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

value="kubernetes"
if (($PLUGINS[(Ie)$value])); then
	alias kube="kubectl"
	source <(kubectl completion zsh)
fi

value="helm"
if (($PLUGINS[(Ie)$value])); then
	source <(helm completion zsh)
fi

value="minikube"
if (($PLUGINS[(Ie)$value])); then
source <(minikube completion zsh)
fi