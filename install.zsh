#!/bin/zsh
echo "setopt interactivecomments" >> ~/.zshrc
echo "" >> ~/.zshrc
echo "export STB_ZSH=${0:a:h}" >> ~/.zshrc
echo "export PLUGINS=() #(kubernetes helm minikube)" >> ~/.zshrc
FILE="~/.config/starship.toml"
if test -f "$FILE"; then
    echo "$FILE exists. Not Updating Config"
else 
    echo "export STARSHIP_CONFIG=\$STB_ZSH/starship.toml" >> ~/.zshrc
fi
echo "source \${STB_ZSH}/zshrc" >> ~/.zshrc
